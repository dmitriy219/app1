import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom'

function App() {
  return (
    <div className="App">
      <Router>
        <nav>
         <h1>App1 Navigation</h1>
          <ul>
            <li>
              <Link to="/app1/page1">Page1</Link>
            </li>
            <li>
              <Link to="/app1/page2">Page2</Link>
            </li>
            <li>
              <Link to="/app1/page3">Page2</Link>
            </li>
          </ul>
        </nav>
        <Switch>
          <Route path="/app1/page1">App1 Page 1</Route>
          <Route path="/app1/page2">App1 Page 2</Route>
          <Route path="/app1/page3">App1 Page 3</Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
